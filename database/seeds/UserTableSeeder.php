<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'Valentina',
            'email' => 'valena.92@gmail.com',
            'phone' => '123',
            'contact_emergency' => '123',
            'rol_id' => '1',
            'password' => bcrypt('ayudap123'),
        ]);
    }
}
