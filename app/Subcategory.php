<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Subcategory extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Contents()
    {
        return $this->hasMany('App\Content', 'subcategory_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Categories()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    //
    protected $table = 'subcategory';

    /**
     * @return SubCategories
     **/
    public static function getSubcategories()
    {
        return DB::table('subcategory')
            ->whereNull('subcategory.deleted_at')
            ->get();
    }

    /**
     * @return Subcategory
     **/
    public static function getSubcategoryById($id)
    {
        return DB::table('subcategory')
            ->where('subcategory.id', '=', $id)
            ->whereNull('subcategory.deleted_at')
            ->get();
    }

    /**
     * @return Category/Subcategories
     **/
    public static function getSubcategoriesByCategoryId($id)
    {
        return DB::table('subcategory')
            ->join('category', 'category.id', '=', 'subcategory.category_id')
            ->where('category.id', '=', $id)
            ->whereNull('subcategory.deleted_at')
            ->get();
    }
}
