<?php 

namespace App\Custom;

use Illuminate\Support\Facades\Auth;

class LoginClass 
{
    public static function validateLogin(){
      if(Auth::check() && Auth::user()->rol_id==1){
        return true;
      } else {
        Auth::logout();
        return false;
      }
    }
}