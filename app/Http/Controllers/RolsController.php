<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Custom\LoginClass;

use App\Rol;

use DB;

class RolsController extends Controller
{
  //
  public function __construct()
  {
    //$this->middleware('auth');
  }


  public function index(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('rols.index', []);
    } else{      
      return redirect('/cms');
    }
  }

  public function create(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('rols.add', [
        []
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function edit(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $rol = Rol::findOrFail($id);
      return view('rols.add', [
        'model' => $rol    ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function show(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $rol = Rol::findOrFail($id);
      return view('rols.show', [
        'model' => $rol    ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function grid(Request $request)
  {
    if(LoginClass::validateLogin()){
      $len = $_GET['length'];
      $start = $_GET['start'];
  
      $select = "SELECT *,1,2 ";
      $presql = " FROM rol a ";
      if($_GET['search']['value']) {
        $presql .= " WHERE rol LIKE '%".$_GET['search']['value']."%' ";
      }
  
      $presql .= "  ";
  
      //------------------------------------
      // 1/2/18 - Jasmine Robinson Added Orderby Section for the Grid Results
      //------------------------------------
      $orderby = "";
      $columns = array('id','rol','created_at','updated_at','deleted_at',);
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');
      $orderby = "Order By " . $order . " " . $dir;
  
      $sql = $select.$presql.$orderby." LIMIT ".$start.",".$len;
      //------------------------------------
  
      $qcount = DB::select("SELECT COUNT(a.id) c".$presql);
      //print_r($qcount);
      $count = $qcount[0]->c;
  
      $results = DB::select($sql);
      $ret = [];
      foreach ($results as $row) {
        $r = [];
        foreach ($row as $value) {
          $r[] = $value;
        }
        $ret[] = $r;
      }
  
      $ret['data'] = $ret;
      $ret['recordsTotal'] = $count;
      $ret['iTotalDisplayRecords'] = $count;
  
      $ret['recordsFiltered'] = count($ret);
      $ret['draw'] = $_GET['draw'];
  
      echo json_encode($ret);
    } else{      
      return redirect('/cms');
    }

  }


  public function update(Request $request) {
    if(LoginClass::validateLogin()){
      //
      /*$this->validate($request, [
      'name' => 'required|max:255',
    ]);*/
    $rol = null;
    if($request->id > 0) { $rol = Rol::findOrFail($request->id); }
    else {
      $rol = new Rol;
    }
  
  
    
      $rol->id = $request->id?:0;
      
    
        $rol->rol = $request->rol;
    
    
        $rol->created_at = $request->created_at;
    
    
        $rol->updated_at = $request->updated_at;
    
    
        $rol->deleted_at = $request->deleted_at;
    
      //$rol->user_id = $request->user()->id;
    $rol->save();
  
    return redirect('/rols');
    } else{      
      return redirect('/cms');
    }

}

public function store(Request $request)
{
  if(LoginClass::validateLogin()){
    return $this->update($request);
  } else{      
    return redirect('/cms');
  }
}

public function destroy(Request $request, $id) {
  if(LoginClass::validateLogin()){

    $rol = Rol::findOrFail($id);
  
    $rol->delete();
    return "OK";
  } else{      
    return redirect('/cms');
  }

}


}
