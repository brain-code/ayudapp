<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Custom\LoginClass;

use App\Log;

use DB;

class LogsController extends Controller
{
  //
  public function __construct()
  {
    //$this->middleware('auth');
  }


  public function index(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('logs.index', []);
    } else{      
      return redirect('/cms');
    }
  }

  public function create(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('logs.add', [
        []
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function edit(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $log = Log::findOrFail($id);
      return view('logs.add', [
        'model' => $log    ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function show(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $log = Log::findOrFail($id);
      return view('logs.show', [
        'model' => $log    ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function grid(Request $request)
  {
    if(LoginClass::validateLogin()){
      $len = $_GET['length'];
      $start = $_GET['start'];
  
      $select = "SELECT *,1,2 ";
      $presql = " FROM log a ";
      if($_GET['search']['value']) {
        $presql .= " WHERE data LIKE '%".$_GET['search']['value']."%' ";
      }
  
      $presql .= "  ";
  
      //------------------------------------
      // 1/2/18 - Jasmine Robinson Added Orderby Section for the Grid Results
      //------------------------------------
      $orderby = "";
      $columns = array('id','data','user_id','created_at','updated_at','deleted_at',);
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');
      $orderby = "Order By " . $order . " " . $dir;
  
      $sql = $select.$presql.$orderby." LIMIT ".$start.",".$len;
      //------------------------------------
  
      $qcount = DB::select("SELECT COUNT(a.id) c".$presql);
      //print_r($qcount);
      $count = $qcount[0]->c;
  
      $results = DB::select($sql);
      $ret = [];
      foreach ($results as $row) {
        $r = [];
        foreach ($row as $value) {
          $r[] = $value;
        }
        $ret[] = $r;
      }
  
      $ret['data'] = $ret;
      $ret['recordsTotal'] = $count;
      $ret['iTotalDisplayRecords'] = $count;
  
      $ret['recordsFiltered'] = count($ret);
      $ret['draw'] = $_GET['draw'];
  
      echo json_encode($ret);
    } else{      
      return redirect('/cms');
    }

  }


  public function update(Request $request) {
    if(LoginClass::validateLogin()){
      //
      /*$this->validate($request, [
      'name' => 'required|max:255',
    ]);*/
    $log = null;
    if($request->id > 0) { $log = Log::findOrFail($request->id); }
    else {
      $log = new Log;
    }
  
  
    
      $log->id = $request->id?:0;
      
    
        $log->data = $request->data;
    
    
        $log->user_id = $request->user_id;
    
    
        $log->created_at = $request->created_at;
    
    
        $log->updated_at = $request->updated_at;
    
    
        $log->deleted_at = $request->deleted_at;
    
      //$log->user_id = $request->user()->id;
    $log->save();
  
    return redirect('/logs');
    } else{      
      return redirect('/cms');
    }

}

public function store(Request $request)
{
  if(LoginClass::validateLogin()){
    return $this->update($request);
  } else{      
    return redirect('/cms');
  }
}

public function destroy(Request $request, $id) {
  if(LoginClass::validateLogin()){

    $log = Log::findOrFail($id);
  
    $log->delete();
    return "OK";
  } else{      
    return redirect('/cms');
  }

}


}
