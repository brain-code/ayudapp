<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Custom\LoginClass;

use App\Subcategory;

use DB;

class SubcategoriesController extends Controller
{
  //
  public function __construct()
  {
    //$this->middleware('auth');
  }


  public function index(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('subcategories.index', []);
    } else{      
      return redirect('/cms');
    }
  }

  public function create(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('subcategories.add', [
        []
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function edit(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $subcategory = Subcategory::findOrFail($id);
      return view('subcategories.add', [
        'model' => $subcategory
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function show(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $subcategory = Subcategory::findOrFail($id);
      return view('subcategories.show', [
        'model' => $subcategory
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function grid(Request $request)
  {
    if(LoginClass::validateLogin()){
      $len = $_GET['length'];
      $start = $_GET['start'];
  
      $select = "SELECT *,1,2 ";
      $presql = " FROM subcategory a ";
      if ($_GET['search']['value']) {
        $presql .= " WHERE subcategory LIKE '%" . $_GET['search']['value'] . "%' ";
      }
  
      $presql .= "  ";
  
      //------------------------------------
      // 1/2/18 - Jasmine Robinson Added Orderby Section for the Grid Results
      //------------------------------------
      $orderby = "";
      $columns = array('id', 'subcategory', 'category_id', 'created_at', 'updated_at', 'deleted_at',);
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');
      $orderby = "Order By " . $order . " " . $dir;
  
      $sql = $select . $presql . $orderby . " LIMIT " . $start . "," . $len;
      //------------------------------------
  
      $qcount = DB::select("SELECT COUNT(a.id) c" . $presql);
      //print_r($qcount);
      $count = $qcount[0]->c;
  
      $results = DB::select($sql);
      $ret = [];
      foreach ($results as $row) {
        $r = [];
        foreach ($row as $value) {
          $r[] = $value;
        }
        $ret[] = $r;
      }
  
      $ret['data'] = $ret;
      $ret['recordsTotal'] = $count;
      $ret['iTotalDisplayRecords'] = $count;
  
      $ret['recordsFiltered'] = count($ret);
      $ret['draw'] = $_GET['draw'];
  
      echo json_encode($ret);
    } else{      
      return redirect('/cms');
    }
  }


  public function update(Request $request)
  {
    if(LoginClass::validateLogin()){
      //
      /*$this->validate($request, [
      'name' => 'required|max:255',
    ]);*/
      $subcategory = null;
      if ($request->id > 0) {
        $subcategory = Subcategory::findOrFail($request->id);
      } else {
        $subcategory = new Subcategory;
      }
  
  
  
      $subcategory->id = $request->id ?: 0;
  
  
      $subcategory->subcategory = $request->subcategory;
  
  
      $subcategory->category_id = $request->category_id;
  
  
      $subcategory->created_at = $request->created_at;
  
  
      $subcategory->updated_at = $request->updated_at;
  
  
      $subcategory->deleted_at = $request->deleted_at;
  
      //$subcategory->user_id = $request->user()->id;
      $subcategory->save();
  
      return redirect('/subcategories');
    } else{      
      return redirect('/cms');
    }
  }

  public function store(Request $request)
  {
    if(LoginClass::validateLogin()){
      return $this->update($request);
    } else{      
      return redirect('/cms');
    }
  }

  public function destroy(Request $request, $id)
  {
    if(LoginClass::validateLogin()){

      $subcategory = Subcategory::findOrFail($id);
  
      $subcategory->delete();
      return "OK";
    } else{      
      return redirect('/cms');
    }
  }

  /*
   * Services
   * 
  */

  public function getSubcategories(Request $request)
  {
    $cats = Subcategory::getSubcategories();
    if ($cats->isEmpty()) {
      $cats = null;
    }
    return Response::json($cats);
  }

  public function getSubcategoryById(Request $request)
  {
    $input = $request->all();
    $cat = Subcategory::getSubcategoryById($input['id']);
    if ($cat->isEmpty()) {
      $cat = null;
    }
    return Response::json($cat);
  }

  public function getSubcategoriesByCategoryId(Request $request)
  {
    $input = $request->all();
    $cat = Subcategory::getSubcategoriesByCategoryId($input['id']);
    if ($cat->isEmpty()) {
      $cat = null;
    }
    return Response::json($cat);
  }

}
