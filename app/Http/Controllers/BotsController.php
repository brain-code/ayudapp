<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bot;
use App\User;

use DB;

class BotsController extends Controller
{
  //
  public function __construct()
  {
    //$this->middleware('auth');
  }


  public function index(Request $request)
  {
    return view('bots.index', []);
  }

  public function create(Request $request)
  {
    return view('bots.add', [
      []
    ]);
  }

  public function edit(Request $request, $id)
  {
    $bot = Bot::findOrFail($id);
    return view('bots.add', [
      'model' => $bot    ]);
  }

  public function show(Request $request, $id)
  {
    $bot = Bot::findOrFail($id);
    return view('bots.show', [
      'model' => $bot    ]);
  }

  public function grid(Request $request)
  {
    $len = $_GET['length'];
    $start = $_GET['start'];

    $select = "SELECT *,1,2 ";
    $presql = " FROM bot a ";
    if($_GET['search']['value']) {
      $presql .= " WHERE user_id LIKE '%".$_GET['search']['value']."%' ";
    }

    $presql .= "  ";

    //------------------------------------
    // 1/2/18 - Jasmine Robinson Added Orderby Section for the Grid Results
    //------------------------------------
    $orderby = "";
    $columns = array('id','user_id','conversation_id','date','context',);
    $order = $columns[$request->input('order.0.column')];
    $dir = $request->input('order.0.dir');
    $orderby = "Order By " . $order . " " . $dir;

    $sql = $select.$presql.$orderby." LIMIT ".$start.",".$len;
    //------------------------------------

    $qcount = DB::select("SELECT COUNT(a.id) c".$presql);
    //print_r($qcount);
    $count = $qcount[0]->c;

    $results = DB::select($sql);
    $ret = [];
    foreach ($results as $row) {
      $r = [];
      foreach ($row as $value) {
        $r[] = $value;
      }
      $ret[] = $r;
    }

    $ret['data'] = $ret;
    $ret['recordsTotal'] = $count;
    $ret['iTotalDisplayRecords'] = $count;

    $ret['recordsFiltered'] = count($ret);
    $ret['draw'] = $_GET['draw'];

    echo json_encode($ret);

  }


  public function update(Request $request) {
    //
    /*$this->validate($request, [
    'name' => 'required|max:255',
  ]);*/
  $bot = null;
  if($request->id > 0) { $bot = Bot::findOrFail($request->id); }
  else {
    $bot = new Bot;
  }


  
    $bot->id = $request->id?:0;
    
  
      $bot->user_id = $request->user_id;
  
  
      $bot->conversation_id = $request->conversation_id;
  
  
      $bot->date = $request->date;
  
  
      $bot->context = $request->context;
  
    //$bot->user_id = $request->user()->id;
  $bot->save();

  return redirect('/bots');

}

public function store(Request $request)
{
  return $this->update($request);
}

public function destroy(Request $request, $id) {

  $bot = Bot::findOrFail($id);

  $bot->delete();
  return "OK";

}
public function send(Request $request) {
  /*  By zeuz ;P */
  $wurl="https://gateway-syd.watsonplatform.net/assistant/api/v1/workspaces/53b5fc24-c820-47a6-9c21-87417db5e2f7/message?version=2018-09-20";
    $wapi="KPWqoS4CZoIIdiYEbnpG1jsbFeDf1JqU2lQPrZR5NY3b";
    //$wapi="i1msUZvx661W6Je3rbyJr4g9QcG1buhoF1_KMvC3NUht";
    //KPWqoS4CZoIIdiYEbnpG1jsbFeDf1JqU2lQPrZR5NY3b
    $user=User::findOrFail($request->idu);
    $wcid=Bot::where('user_id',"=",$request->idu)->first();
    $postData = array( 'input' => array("text"=> $request->msg) );
    if($wcid){
        $postData["context"]=json_decode($wcid->context);
        //$result["contextv"]=1;
        //$result["contextt"]=$wcid->context;
        $result["uname"]=$user->name;
    }else{
        $postData["context"]= new \stdClass;
    }
    /*if(isset($user->name)){
        $postData["context"]->usrname=$user->name;
    }
    if(isset($user->phone)){
      $postData["context"]->usrphn=$user->phone;
    }*/

    $handle = curl_init();
    
    
    /*echo "<pre>";
    print_r($postData);
    echo "</pre>";
    exit;*/
    $headers[]  = 'Content-Type: application/json';
    curl_setopt_array($handle,
        array(
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $wurl,
            CURLOPT_USERPWD  => "apikey:".$wapi,                            
            CURLOPT_POST     => true,
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_RETURNTRANSFER     => true,
        )
    );
        
    $respuesta = curl_exec($handle);
    $wresult=json_decode($respuesta);
    curl_close($handle);

    if(!$wcid){
      $wcid=new Bot();
      $wcid->user_id=$user->id;
      $wcid->conversation_id=$wresult->context->conversation_id;
    }
  $wcid->context=json_encode($wresult->context);
  if($wcid->save()){
    //echo "save ok"  ;
  }else{
      echo "serror";
      var_dump($wcid->errors);
  }

    //print_r($wresult->context->conversation_id);
      /*echo "<pre>";
    print_r($wresult);
    echo "</pre>";
*/
    
        //print_r($wresult);
        $result["success"]=1;
       
        
       // $result["res"]["id"]=$data->id;
        $result["message"]=$wresult->output->text[0];
        if(count($wresult->output->text)>1){
            $result["message2"]=$wresult->output->text[1];
            if(count($wresult->output->text)>2){
                $result["message3"]==$wresult->output->text[2];
            }
        }
        $vars=$wresult->context;
        unset($vars->conversation_id);
        unset($vars->system);
        

          $result["vars"]=$vars;
        
        //$result["res"]["waid"]=$wresult;
        //$result["res"]["tmsg"]=count($wresult->output->generic);
       /* echo "<pre>";
        print_r($result);
        echo "</pre>";*/
        return $result;

  
}


}
