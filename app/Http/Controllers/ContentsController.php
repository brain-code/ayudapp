<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Custom\LoginClass;

use App\Content;

use DB;

class ContentsController extends Controller
{
  //
  public function __construct()
  {
    //$this->middleware('auth');
  }


  public function index(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('contents.index', []);
    } else{      
      return redirect('/cms');
    }
  }

  public function create(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('contents.add', [
        []
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function edit(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $content = Content::findOrFail($id);
      return view('contents.add', [
        'model' => $content
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function show(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $content = Content::findOrFail($id);
      return view('contents.show', [
        'model' => $content
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function grid(Request $request)
  {
    if(LoginClass::validateLogin()){
      $len = $_GET['length'];
      $start = $_GET['start'];
  
      $select = "SELECT *,1,2 ";
      $presql = " FROM content a ";
      if ($_GET['search']['value']) {
        $presql .= " WHERE title LIKE '%" . $_GET['search']['value'] . "%' ";
      }
  
      $presql .= "  ";
  
      //------------------------------------
      // 1/2/18 - Jasmine Robinson Added Orderby Section for the Grid Results
      //------------------------------------
      $orderby = "";
      $columns = array('id', 'title', 'type', 'subcategory_id', 'created_at', 'updated_at', 'deleted_at',);
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');
      $orderby = "Order By " . $order . " " . $dir;
  
      $sql = $select . $presql . $orderby . " LIMIT " . $start . "," . $len;
      //------------------------------------
  
      $qcount = DB::select("SELECT COUNT(a.id) c" . $presql);
      //print_r($qcount);
      $count = $qcount[0]->c;
  
      $results = DB::select($sql);
      $ret = [];
      $temp = '';
      foreach ($results as $key => $row) {
        $r = [];
        foreach ($row as $value) {    
          $r[] = $value;
        }
        $ret[] = $r;
      }
  
      $ret['data'] = $ret;
      $ret['recordsTotal'] = $count;
      $ret['iTotalDisplayRecords'] = $count;
  
      $ret['recordsFiltered'] = count($ret);
      $ret['draw'] = $_GET['draw'];
  
      echo json_encode($ret);
    } else{      
      return redirect('/cms');
    }
  }


  public function update(Request $request)
  {
    if(LoginClass::validateLogin()){
      //
      /*$this->validate($request, [
      'name' => 'required|max:255',
    ]);*/
      $content = null;
      if ($request->id > 0) {
        $content = Content::findOrFail($request->id);
      } else {
        $content = new Content;
      }
  
  
  
      $content->id = $request->id ?: 0;
  
  
      $content->resource = $request->resource;
  
      $content->title = $request->title;
  
  
      $content->type = $request->type;
  
  
      $content->subcategory_id = $request->subcategory_id;
  
  
      $content->created_at = $request->created_at;
  
  
      $content->updated_at = $request->updated_at;
  
  
      $content->deleted_at = $request->deleted_at;
  
      //$content->user_id = $request->user()->id;
      $content->save();
  
      return redirect('/contents');
    } else{      
      return redirect('/cms');
    }
  }

  public function store(Request $request)
  {
    if(LoginClass::validateLogin()){
      return $this->update($request);
    } else{      
      return redirect('/cms');
    }
  }

  public function destroy(Request $request, $id)
  {
    if(LoginClass::validateLogin()){

      $content = Content::findOrFail($id);
  
      $content->delete();
      return "OK";
    } else{      
      return redirect('/cms');
    }
  }

  public function getContentById(Request $request)
  {
    $input = $request->all();
    $content = Content::getContentById($input['id']);
    if ($content->isEmpty()) {
      $content = null;
    }
    return Response::json($content);
  }

  public function getContentBySubcategoryId(Request $request)
  {
    $input = $request->all();
    $content = Content::getContentBySubcategoryId($input['id']);
    if ($content->isEmpty()) {
      $content = null;
    }
    return Response::json($content);
  }
}
