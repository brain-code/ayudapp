<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Custom\LoginClass;
use Carbon\Carbon;


use App\User;
use Hash;

use Response;
use DB;

class UsersController extends Controller
{
  //
  public function __construct()
  {
  }


  public function index(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('users.index', []);
    } else{      
      return redirect('/cms');
    }
  }

  public function create(Request $request)
  {
    if(LoginClass::validateLogin()){
      return view('users.add', [
        []
      ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function edit(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $user = User::findOrFail($id);
      return view('users.add', [
        'model' => $user    ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function show(Request $request, $id)
  {
    if(LoginClass::validateLogin()){
      $user = User::findOrFail($id);
      return view('users.show', [
        'model' => $user    ]);
    } else{      
      return redirect('/cms');
    }
  }

  public function grid(Request $request)
  {
    if(LoginClass::validateLogin()){
      $len = $_GET['length'];
      $start = $_GET['start'];
  
      $select = "SELECT *,1,2 ";
      $presql = " FROM user a ";
      if($_GET['search']['value']) {
        $presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' ";
      }
  
      $presql .= "  ";
  
      //------------------------------------
      // 1/2/18 - Jasmine Robinson Added Orderby Section for the Grid Results
      //------------------------------------
      $orderby = "";
      $columns = array('id','name','email','phone','contact_emergency','date','tyc','rol_id','created_at','updated_at','deleted_at','remember_token',);
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');
      $orderby = "Order By " . $order . " " . $dir;
  
      $sql = $select.$presql.$orderby." LIMIT ".$start.",".$len;
      //------------------------------------
  
      $qcount = DB::select("SELECT COUNT(a.id) c".$presql);
      //print_r($qcount);
      $count = $qcount[0]->c;
  
      $results = DB::select($sql);
      $ret = [];
      foreach ($results as $row) {
        $r = [];
        foreach ($row as $value) {
          $r[] = $value;
        }
        $ret[] = $r;
      }
  
      $ret['data'] = $ret;
      $ret['recordsTotal'] = $count;
      $ret['iTotalDisplayRecords'] = $count;
  
      $ret['recordsFiltered'] = count($ret);
      $ret['draw'] = $_GET['draw'];
  
      echo json_encode($ret);
    } else{      
      return redirect('/cms');
    }

  }


  public function update(Request $request) {
    if(LoginClass::validateLogin()){
      //
      /*$this->validate($request, [
      'name' => 'required|max:255',
    ]);*/
    $user = null;
    if($request->id > 0) { $user = User::findOrFail($request->id); }
    else {
      $user = new User;
    }
  
  
    
      $user->id = $request->id?:0;
      
    
        $user->name = $request->name;
    
    
        $user->email = $request->email;
    
    
        $user->phone = $request->phone;
    
    
        $user->contact_emergency = $request->contact_emergency;
    
    
        $user->date = $request->date;
    
    
        $user->tyc = $request->tyc;
    
    
        $user->rol_id = $request->rol_id;
    
    
        $user->created_at = $request->created_at;
    
    
        $user->updated_at = $request->updated_at;
    
    
        $user->deleted_at = $request->deleted_at;
    
    
        $user->remember_token = $request->remember_token;
        $user->password = $request->password;
    
      //$user->user_id = $request->user()->id;
    $user->save();
  
    return redirect('/users');
    } else{      
      return redirect('/cms');
    }

  }

  public function store(Request $request)
  {
    if(LoginClass::validateLogin()){
      return $this->update($request);
    } else{      
      return redirect('/cms');
    }
  }

  public function destroy(Request $request, $id) {
    if(LoginClass::validateLogin()){

      $user = User::findOrFail($id);
    
      $user->delete();
      return "OK";
    } else{      
      return redirect('/cms');
    }

  }

  public function registerUsers(Request $request){

    //Get data request
    if ($request->input('email')!=null) {
      if (strip_tags($request->input('email'))!='' && trim($request->input('email'))) {
        $email = trim($request->input('email'));
      }
    }
    if ($request->input('phone')!=null) {
      if (strip_tags($request->input('phone'))!='' && trim($request->input('phone'))) {
        $phone = trim($request->input('phone'));
      }
    }
    $user = User::findByEmailAndPhone($email, $phone);
    if(!$user){
      $user = new User();
      $user->email = $email;
      $user->phone = $phone;
      if ($request->input('name')!=null) {
        if (strip_tags($request->input('name'))!='' && trim($request->input('name'))) {
          $user->name = trim($request->input('name'));
        }
      }
      if (strip_tags($request->input('date'))!='' && trim($request->input('date'))) {
        $user->date =  trim($request->input('date'));
      }
      if ($request->input('password')!=null) {
        if (strip_tags($request->input('password'))!='' && trim($request->input('password'))) {
          $user->password = $request->input('password');
        }
      }
      if ($request->input('tyc')!=null) {
        if (strip_tags($request->input('tyc'))!='' && trim($request->input('tyc'))) {
          $user->tyc = $request->input('tyc');
        }
      }
      $user->created_at = Carbon::now();
      $user->remember_token = str_random(10);
      $user->rol_id = 2;
      try {
        $user->save();    

        $result = array(
          'success' => true,
          'data' => array(
            'celular' => $user->phone,
            'id' => $user->id,
            'token' => $user->remember_token
          ),
          'message' => "Se ha realizado el registro satisfactoriamente"
        );
      } catch (Exception $e) {  
        $result = array(
          'success' => false,
          'data' => array(),
          'message' => "Se ha producido un error guardando la información. Por favor intentelo nuevamente"
        );
      }
    } else{
      $result = array(
        'success' => false,
        'data' => array(),
        'message' => "El correo electrónico y/o teléfono proporcionados ya esta siendo usado por otro usuario"
      );
    }
    return Response::json($result);
  }

  public function loginUsers(Request $request){

    //Get data request
    if ($request->input('password')!=null) {
      if (strip_tags($request->input('password'))!='' && trim($request->input('password'))) {
        $password = trim($request->input('password'));
      }
    }
    if ($request->input('phone')!=null) {
      if (strip_tags($request->input('phone'))!='' && trim($request->input('phone'))) {
        $phone = trim($request->input('phone'));
      }
    }
    $user = User::findByPhoneAndPassword($password, $phone);
    if($user){      
      try {
        $user->save();    
        $result = array(
          'success' => true,
          'data' => array(
            'phone' => $user->phone,
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'token' => $user->remember_token
          ),
          'message' => "Se ha iniciado seseión satisfactoriamente"
        );
      } catch (Exception $e) {  
        $result = array(
          'success' => false,
          'data' => array(),
          'message' => "Se ha producido un error al iniciar sesión. Por favor intentelo nuevamente"
        );
      }
    } else{
      $result = array(
        'success' => false,
        'data' => array(),
        'message' => "No se reconoce la información de inicio de sesión"
      );
    }
    return Response::json($result);
  }

  public function updateProfile(Request $request){

    //Get data request
    if ($request->input('token')!=null) {
      if (strip_tags($request->input('token'))!='' && trim($request->input('token'))) {
        $token = trim($request->input('token'));
      }
    }
    if ($request->input('phone')!=null) {
      if (strip_tags($request->input('phone'))!='' && trim($request->input('phone'))) {
        $phone = trim($request->input('phone'));
      }
    }
    $user = User::findByPhoneAndToken($token, $phone);
    if($user){
      if ($request->input('name')!=null) {
        if (strip_tags($request->input('name'))!='' && trim($request->input('name'))) {
          $user->name = trim($request->input('name'));
        }
      }
      if (strip_tags($request->input('date'))!='' && trim($request->input('date'))) {
        $user->date =  trim($request->input('date'));
      }
      if ($request->input('password')!=null) {
        if (strip_tags($request->input('password'))!='' && trim($request->input('password'))) {
          $user->password = $request->input('password');
        }
      }
      if ($request->input('contact_emergency')!=null) {
        if (strip_tags($request->input('contact_emergency'))!='' && trim($request->input('contact_emergency'))) {
          $user->contact_emergency = $request->input('contact_emergency');
        }
      }
      try {
        $user->save();    
        $result = array(
          'success' => true,
          'data' => $user,
          'message' => "Se ha actualizado la información satisfactoriamente"
        );
      } catch (Exception $e) {  
        $result = array(
          'success' => false,
          'data' => array(),
          'message' => "Se ha producido un error al actualizar la inforamción. Por favor intentelo nuevamente"
        );
      }
    } else{
      $result = array(
        'success' => false,
        'data' => array(),
        'message' => "No se reconoce la información de inicio de sesión. Por favor acceda de nueva"
      );
    }
    return Response::json($result);
  }

  public function getUserData(Request $request){

    //Get data request
    if ($request->input('token')!=null) {
      if (strip_tags($request->input('token'))!='' && trim($request->input('token'))) {
        $token = trim($request->input('token'));
      }
    }
    if ($request->input('phone')!=null) {
      if (strip_tags($request->input('phone'))!='' && trim($request->input('phone'))) {
        $phone = trim($request->input('phone'));
      }
    }
    $user = User::findByPhoneAndToken($token, $phone);
    if($user){
      try {   
        $result = array(
          'success' => true,
          'data' => $user,
          'message' => "Se ha iniciado seseión satisfactoriamente"
        );
      } catch (Exception $e) {  
        $result = array(
          'success' => false,
          'data' => array(),
          'message' => "Se ha producido un error al iniciar sesión. Por favor intentelo nuevamente"
        );
      }
    } else{
      $result = array(
        'success' => false,
        'data' => array(),
        'message' => "No se reconoce la información de inicio de sesión"
      );
    }
    return Response::json($result);
  }

  public function logoutUser(Request $request){

    //Get data request
    if ($request->input('token')!=null) {
      if (strip_tags($request->input('token'))!='' && trim($request->input('token'))) {
        $token = trim($request->input('token'));
      }
    }
    if ($request->input('phone')!=null) {
      if (strip_tags($request->input('phone'))!='' && trim($request->input('phone'))) {
        $phone = trim($request->input('phone'));
      }
    }
    $user = User::findByPhoneAndToken($token, $phone);
    if($user){
      
      $user->remember_token = "";
      try {
        $user->save();    
        $result = array(
          'success' => true,
          'data' => array(),
          'message' => "Se ha cerrado seseión satisfactoriamente"
        );
      } catch (Exception $e) {  
        $result = array(
          'success' => false,
          'data' => array(),
          'message' => "Se ha producido un error al cerrar sesión. Por favor intentelo nuevamente"
        );
      }
    } else{
      $result = array(
        'success' => false,
        'data' => array(),
        'message' => "No se reconoce la información del usuario"
      );
    }
    return Response::json($result);
  }
}
