<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    //
    protected $table = 'category';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Subcategories()
    {
        return $this->hasMany('App\Subcategory', 'subcategory_id');
    }


    /**
     * @return Categories
     **/
    public static function getCategories() {
        return DB::table('category')
            ->whereNull('category.deleted_at')
            ->get();
    }

    /**
     * @return CategoryById
     **/
    public static function getCategoryById($id) {
        return DB::table('category')
            ->where('category.id', '=', $id)
            ->whereNull('category.deleted_at')
            ->get();
    }
}
