<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Content extends Model
{
    //
    protected $table = 'content';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Subcategories()
    {
        return $this->belongsTo('App\Subcategory', 'subcategory_id');
    }

    /**
     * @return Content
     **/
    public static function getContentById($id)
    {
        return DB::table('content')
            ->where('content.id', '=', $id)
            ->whereNull('content.deleted_at')
            ->get();
    }

    /**
     * @return Subcategory/Contents
     **/
    public static function getContentBySubcategoryId($id)
    {
        return DB::table('content')
            ->join('subcategory', 'subcategory.id', '=', 'content.subcategory_id')
            ->where('subcategory.id', '=', $id)
            ->whereNull('content.deleted_at')
            ->get();
    }
}
