<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('bot_send', 'BotsController@send');

//Categories
Route::get('getCategories', 'CategoriesController@getCategories');
Route::post('getCategoryById', 'CategoriesController@getCategoryById');

//Subcategories
Route::get('getSubcategories', 'SubcategoriesController@getSubcategories');
Route::post('getSubcategoryById', 'SubcategoriesController@getSubcategoryById');
Route::post('getSubcategoriesByCategoryId', 'SubcategoriesController@getSubcategoriesByCategoryId');

//Contents
Route::post('getContentById', 'ContentsController@getContentById');
Route::post('getContentBySubcategoryId', 'ContentsController@getContentBySubcategoryId');

//Users
Route::post('registerUsers', 'UsersController@registerUsers');
Route::post('loginUsers', 'UsersController@loginUsers');
Route::get('getUserData', 'UsersController@getUserData');
Route::post('updateProfile', 'UsersController@updateProfile');
Route::post('logoutUser', 'UsersController@logoutUser');