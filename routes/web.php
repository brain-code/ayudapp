<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/categories/grid', 'CategoriesController@grid');
Route::resource('/categories', 'CategoriesController');
Route::get('/subcategories/grid', 'SubcategoriesController@grid');
Route::resource('/subcategories', 'SubcategoriesController');
Route::get('/contents/grid', 'ContentsController@grid');
Route::resource('/contents', 'ContentsController');
Route::get('/bots/grid', 'BotsController@grid');
Route::resource('/bots', 'BotsController');
Route::get('/logs/grid', 'LogsController@grid');
Route::resource('/logs', 'LogsController');
Route::get('/rols/grid', 'RolsController@grid');
Route::resource('/rols', 'RolsController');
Route::get('/users/grid', 'UsersController@grid');
Route::resource('/users', 'UsersController');
Auth::routes();

Route::get('/cms', 'HomeController@index')->name('home');
