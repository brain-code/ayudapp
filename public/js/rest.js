$(document).ready(function (e) {

    /*Globals*/
    var serviceRest = function (method, url, data) {
        $.LoadingOverlay("show");
        var response;
        $.ajax({
            method: method,
            url: url,
            dataType: "json",
            data: data,
            async: false
        }).done(function (msg) {
            msg != undefined ? response = msg : response = false;
            setTimeout(function () {
                $.LoadingOverlay("hide");
            }, 1000);
        }).fail(function (msg) {
            msg != undefined ? response = msg : response = false;
            setTimeout(function () {
                $.LoadingOverlay("hide");
            }, 1000);
        });

        return response;
    }

    var setStorage = function (variable, data) {
        localStorage.setItem(variable, JSON.stringify(data));
    }

    var getStorage = function (variable) {
        return JSON.parse(localStorage.getItem(variable));
    }

    function encrypt(value) {
        var result = "";
        for (i = 0; i < value.length; i++) {
            if (i < value.length - 1) {
                result += value.charCodeAt(i) + 10;
                result += "-";
            }
            else {
                result += value.charCodeAt(i) + 10;
            }
        }
        return result;
    }
    function decrypt(value) {
        if (value == "" || value == undefined || value == null) {
            return false;
        } else {
            var result = "";
            var array = value.split("-");

            for (i = 0; i < array.length; i++) {
                result += String.fromCharCode(array[i] - 10);
            }
            return result;
        }
    }

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    /*Globals*/

    /*Login*/
    $('#loginForm').submit(function () {
        //var _token = $('input[name="_token"]').val();
        let data = $('#loginForm').serialize();
        let response = serviceRest("post", "/api/loginUsers", data);
        if (response.success) {
            document.cookie = 'utoken=' + encrypt(response.data.token) + '; path=/;';
            setStorage("u", {
                token: response.data.token,
                phone: response.data.phone,
                name: response.data.name,
                email: response.data.email,
                id: response.data.id
            }
            );
            toastr.clear();
            toastr.success(response.message);
            window.location.href = '/home.html';
        } else {
            toastr.clear();
            toastr.warning(response.message);
        }
        return false;
    });

    /*Register*/
    $('#registerForm').submit(function () {
        //var _token = $('input[name="_token"]').val();
        let data = $('#registerForm').serialize();
        let response = serviceRest("post", "/api/registerUsers", data);
        if (response.success) {
            toastr.clear();
            toastr.success(response.message);
            window.location.href = '/login.html';
        } else {
            toastr.clear();
            toastr.warning(response.message);
        }
        return false;
    });

    /*Update Profile*/
    $('#updateForm').submit(function () {
        //var _token = $('input[name="_token"]').val();
        let data = $('#updateForm').serialize();
        data = data + '&token=' + getStorage('u').token;
        let response = serviceRest("post", "/api/updateProfile", data);
        if (response.success) {
            toastr.clear();
            toastr.success(response.message);
        } else {
            toastr.clear();
            toastr.warning(response.message);
        }
        return false;
    });

    /*Chat Form*/
    $('#chatForm').submit(function () {
        //var _token = $('input[name="_token"]').val();
        let data = 'idu=' + getStorage('u').id;
        data = data + '&msg=' + $('#chat-box').val();
        let response = serviceRest("get", "/api/bot_send", data);
        if (response.success) {
            $('.content-conversation').append('<div class="conversacion-user countchat">' + $('#chat-box').val() + '</div>');
            $('.content-conversation').append('<div class="conversacion-boot countchat">' + response.message + '</div>');            
            $('#chat-box').val('');
            var ct = 0;
            $('.countchat').each(function () { ct++; });
            if (ct == 8) {
                $('.content-conversation').css('padding-bottom', '250px');
                $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            }
            if (response.vars.levantalamano) {
                response.vars.levantalamano == "si" ? window.location.href = '/levanta-la-mano.html' : "";
            }
        } else {
            toastr.clear();
            toastr.warning(response.message);
        }
        return false;
    });

    if (window.location.pathname != '/login.html'
        && window.location.pathname != '/registro.html') {
        if (getStorage('u') == null || getStorage('u') == undefined || getStorage('u') == "") {
            // window.location.href = '/login.html';
        } else {
            if (decrypt(getCookie('utoken')) != getStorage('u').token) {
                //   window.location.href = '/login.html';
            }
        }
    }

    if (window.location.pathname == '/perfil.html') {
        $('#nameUpdate').text(getStorage('u').name);
        $('#emailUpdate').text(getStorage('u').email);
        $('#name-perfil').val(getStorage('u').name);
        $('#cel-perfil').val(getStorage('u').phone);
    }

    $('#logout').click(function () {
        localStorage.removeItem('u');
        document.cookie = 'utoken=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        window.location.href = '/login.html';
    });
});