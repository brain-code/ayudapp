var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');

gulp.task('sass-cmarp', function(){
 return gulp.src('src/scss/main.scss')
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(cleanCSS())
  .pipe(sourcemaps.write('./', {
   includeContent: false,
   sourceRoot: 'src/scss/main.scss'
  }))
  .pipe(gulp.dest('assets/css'))
});

gulp.task('uglify', function(){
 return gulp.src('src/js/*.js')
  .pipe(uglify())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('assets/js'));
});

gulp.task('default', function(){
 gulp.start('sass-cmarp');
 gulp.start('uglify');
 gulp.watch('src/scss/**/*.scss', ['sass-cmarp']);
 gulp.watch('src/js/*.js', ['uglify']);
});
