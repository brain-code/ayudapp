@extends('layouts.master')

@section('content')



<h2 class="page-header">Bot</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Bot    </div>

    <div class="panel-body">
                

        <form action="{{ url('/bots') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="user_id" class="col-sm-3 control-label">User Id</label>
            <div class="col-sm-6">
                <input type="text" name="user_id" id="user_id" class="form-control" value="{{$model['user_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="conversation_id" class="col-sm-3 control-label">Conversation Id</label>
            <div class="col-sm-6">
                <input type="text" name="conversation_id" id="conversation_id" class="form-control" value="{{$model['conversation_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="date" class="col-sm-3 control-label">Date</label>
            <div class="col-sm-6">
                <input type="text" name="date" id="date" class="form-control" value="{{$model['date'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="context" class="col-sm-3 control-label">Context</label>
            <div class="col-sm-6">
                <input type="text" name="context" id="context" class="form-control" value="{{$model['context'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/bots') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection