@extends('layouts.master')

@section('content')


<h2 class="page-header">User</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify User    </div>

    <div class="panel-body">
                
        <form action="{{ url('/users'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif
                              
            <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{isset($model) ?  $model->id : ""}}" readonly="readonly">
                </div>
            </div>                                                                              <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{isset($model) ?  $model->name : ""}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-6">
                    <input type="text" name="email" id="email" class="form-control" value="{{isset($model) ?  $model->email : ""}}">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-6">
                    <input type="password" name="password" id="password" class="form-control" value="{{isset($model) ?  $model->password : ""}}">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-2">
                    <input type="number" name="phone" id="phone" class="form-control" value="{{isset($model) ?  $model->phone : ""}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="contact_emergency" class="col-sm-3 control-label">Contact Emergency</label>
                <div class="col-sm-2">
                    <input type="number" name="contact_emergency" id="contact_emergency" class="form-control" value="{{isset($model) ?  $model->contact_emergency : ""}}">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="date" class="col-sm-3 control-label">Date</label>
                <div class="col-sm-3">
                    <input type="date" name="date" id="date" class="form-control" value="{{isset($model) ?  $model->date : ""}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="tyc" class="col-sm-3 control-label">Tyc</label>
                <div class="col-sm-2">
                    <input type="number" name="tyc" id="tyc" class="form-control" value="{{isset($model) ?  $model->tyc : ""}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="rol_id" class="col-sm-3 control-label">Rol Id</label>
                <div class="col-sm-2">
                    <input type="number" name="rol_id" id="rol_id" class="form-control" value="{{isset($model) ?  $model->rol_id : ""}}">
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/users') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection