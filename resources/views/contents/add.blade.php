@extends('layouts.master')

@section('content')


<h2 class="page-header">Content</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Content    </div>

    <div class="panel-body">
                
        <form action="{{ url('/contents'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{isset($model) ?$model['id'] : ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                    <label for="title" class="col-sm-3 control-label">Title</label>
                    <div class="col-sm-6">
                        <input type="text" name="title" id="title" class="form-control" value="{{isset($model) ?  $model->title : ""}}">
                    </div>
                </div>
                                                                                                                                                <div class="form-group">
                <label for="resource" class="col-sm-3 control-label">Resource</label>
                <div class="col-sm-6">
                    <textarea name="resource" id="resource" class="form-control">{{isset($model) ?$model['resource'] : ''}}</textarea>
                </div>
            </div>
                                                            <div class="form-group">
                <label for="type" class="col-sm-3 control-label">Type</label>
                <div class="col-sm-6">
                        <select name="type" id="type" class="form-control">
                                <option value="Articulo" {{isset($model) ? 'selected' : ''}}>Articulo</option> 
                                <option value="Video" {{isset($model) ? 'selected' : ''}}>Video</option>
                                <option value="Link" {{isset($model) ? 'selected' : ''}}>Link</option>
                        </select>
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="subcategory_id" class="col-sm-3 control-label">Subcategory Id</label>
                <div class="col-sm-2">
                    <input type="number" name="subcategory_id" id="subcategory_id" class="form-control" value="{{isset($model) ?$model['subcategory_id'] : ''}}">
                </div>
            </div>
                                                                                                                        <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{isset($model) ?$model['created_at'] : ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{isset($model) ?$model['updated_at'] : ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="deleted_at" class="col-sm-3 control-label">Deleted At</label>
                <div class="col-sm-6">
                    <input type="text" name="deleted_at" id="deleted_at" class="form-control" value="{{isset($model) ?$model['deleted_at'] : ''}}">
                </div>
            </div>
                        
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/contents') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection